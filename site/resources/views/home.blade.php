@extends('layouts.app')
@section('title' , __('Home '))
@section('content')

<div class="container">
   <a href="{{route('articles.create')}}" class="btn btn-lg btn-outline-warning mt-4 mb-4">{{__('New article')}}</a>
</div>
<hr>
    <div class="row mt-4">
    @forelse($articles as $article)
        <div class="col-md-3">
            <div class="mb-2 card">
                <div class="card-body">
                    <a href="{{route('articles.show' , $article)}}">{{$article->title}}</a>
                </div>
                <div class="card-footer">
                    <a href="{{route('articles.edit' ,$article)}}" class="btn btn-outline-dark">{{__('Edit')}}</a>
                    <form  method="post" action="{{route('articles.destroy',$article)}}" style="display: inline-block">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-outline-danger" onclick="return confirm('{{__('Are you sure?')}}')">{{__('Delete')}}</button>
                    </form>
                </div>
            </div>
        </div>

    @empty
    {{__('sorry you done have any articles yet')}}
    @endforelse
    </div>
@endsection
