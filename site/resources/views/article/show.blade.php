@extends('layouts.app')
@section('title' , $article->title)

@section('content')

    <div class="container ">
    <div class="card">
        <div class="card-header ">{{$article->title}}</div>


    <div class="card-body">
        {!!nl2br($article->content)!!}
    </div>
    {{--disply info about the author --}}
    <div class="card-footer">
        <div><b>{{__('Author')}} </b>: {{$article->user->name}}</div>
        <div><b>{{__('Created at')}}</b>: {{$article->created_at}}</div>
    </div>
    </div>
    {{--show comments under the article --}}
        <h3 class="mt-2 text-warning">{{__('Comments :')}}</h3>

     <div id="comments" class="mt-4 ">
         @forelse($article->comments as $comment)
          <div class="card p-3 mb-2">
              {{$comment->content}}
              <p>{{__('Author')}} : {{$comment->user->name}}</p>
          </div>
         @empty
         {{__('No comment yet')}}
         @endforelse
     </div>

     @auth
    <div id="commentForm " class="mt-5">
        <div class="card ">
            <h5 class="card-header bg-secondary text-white">{{__('Type your comment here')}}</h5>
            <div class="card-body">
                <form action="{{route('comments.store' ,$article->id)}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="content">{{__('Content')}}</label>
                        <textarea class="form-control"
                                  placeholder="{{__('Type your comments')}}"
                                  name="content" id="content" cols="30" rows="10"></textarea>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-outline-success" type="submit">{{__('Save')}}</button>
                    </div>

                </form>
            </div>
        </div>

    </div>
        @else
        <p><a href="{{route('login')}}"> {{__('Log in to comment')}}</a></p>
        @endauth
    </div>
@endsection

