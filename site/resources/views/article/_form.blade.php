<div class="container">
@csrf
<div class="form-group">
    <label for="title">{{__('Title')}}</label>
    <input type="text" name="title" class="form-control" @isset($article) value="{{$article->title}}" @endisset>
</div>
    <div class="form-group">
        @foreach($categories as $id =>$title)
            <label for="category__{{$id}}">{{$title}}</label>
            <input type="checkbox" name="categories[]" value="{{$id}}" id="category__{{$id}}"

            @if(isset($article) && in_array($id ,$articleCategories)) checked @endif
            >
        @endforeach
    </div>

<div class="form-group">
    <label for="content">{{__('Content')}}</label>
    <textarea class="form-control" name="content" id="content" cols="20" rows="10" >@isset($article) {{$article->content}} @endisset</textarea>
</div>

<div class="form-group">
    <button class="btn btn-success">{{$submitText}}</button>
</div>
</div>
