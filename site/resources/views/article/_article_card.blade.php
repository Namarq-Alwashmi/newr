<div class="col-md-4 mb-2">
    <div class="card pb-3">

        <h4 class="card-header" ><a class="text-secondary" href="{{route('articles.show', $article->id)}}">{{$article->title}}</a></h4>
        <div class="card-body">
            {{$article->content}}
        </div>

        <div class="card-footer">
            {{__('Author')}} : {{$article->user->name}}
        </div>
    </div>
</div>
